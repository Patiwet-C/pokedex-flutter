const String API_PATH = 'https://pokeapi.co';
const String API_VERSION = '/api/v2';

const String POKEMON_ENDPOINT = '/pokemon';
const String LOCATION_ENDPOINT = '/location';
const String ITEM_ENDPOINT = '/item';
