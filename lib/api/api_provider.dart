import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:pokedex_flutter/api/apiConfig.dart';
import 'package:pokedex_flutter/constants/statusCode.dart';

class ApiProvider {
  Future<Map<String, dynamic>> getData(String endpoint) async {
    String _path = API_PATH + API_VERSION + endpoint;

    print('path: $_path');
    final response = await Dio().get(_path);
    print('status code: ${response.statusCode}');

    if (response.statusCode == statusCode(StatusCode.code200)) {
      Map<String, dynamic> parsedJson = json.decode(response.toString());
      // debugPrint('response: $parsedJson', wrapWidth: 1024); // enable this when want to log response data
      return parsedJson;
    } else {
      print('Failed to load with status code: ${response.statusCode}');
      throw Exception('Failed to load');
    }
  }

  Future<Map<String, dynamic>> getDataByUrl(String path) async {
    print('path: $path');
    final response = await Dio().get(path);
    print('response status code: ${response.statusCode}');

    if (response.statusCode == statusCode(StatusCode.code200)) {
      Map<String, dynamic> parsedJson = json.decode(response.toString());
      // debugPrint('response: $parsedJson', wrapWidth: 1024); // enable this when want to log response data

      return parsedJson;
    } else {
      print('Failed to load with status code: ${response.statusCode}');
      throw Exception('Failed to load');
    }
  }
}
