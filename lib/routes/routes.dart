import 'package:flutter/material.dart';
import 'package:pokedex_flutter/pages/pokedex/views/move_page.dart';
import 'package:pokedex_flutter/pages/pokedex/views/pokedex_page.dart';
import 'package:pokedex_flutter/pages/pokedex/views/pokemon_page.dart';

class Routes extends StatelessWidget {
  const Routes({Key? key}) : super(key: key);

  Map<String, Widget Function(BuildContext)> _routes(BuildContext context) {
    return {
      'main': (context) => PokedexPage(),
      'pokemon-page': (context) => PokemonPage(),
      'move-page': (context) => MovePage(),
    };
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: _routes(context),
      home: PokedexPage(),
      initialRoute: 'main',
    );
  }
}
