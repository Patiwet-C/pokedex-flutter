enum StatusCode {
  code200,
  code503,
}

int statusCode(StatusCode status) {
  switch (status) {
    case StatusCode.code200:
      return 200;
    case StatusCode.code503:
      return 503;
    default:
      return 404;
  }
}
