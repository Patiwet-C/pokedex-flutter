import 'package:json_annotation/json_annotation.dart';
import 'package:pokedex_flutter/models/commonModel.dart';

@JsonSerializable()
class ContestComboDetail {
  late List<NamedAPIResource>? _useBefore;
  late List<NamedAPIResource>? _useAfter;

  ContestComboDetail(this._useBefore, this._useAfter);

  ContestComboDetail.fromJson(Map<String, dynamic> parsedJson) {
    List<NamedAPIResource>? useBefore = [];
    List<NamedAPIResource>? useAfter = [];

    if (parsedJson['use_before'] != null) {
      for (int i = 0; i < parsedJson['use_before'].length; i++) {
        NamedAPIResource item =
            NamedAPIResource.fromJson(parsedJson['use_before'][i]);
        useBefore.add(item);
      }
    } else {
      useBefore = null;
    }

    if (parsedJson['use_after'] != null) {
      for (int i = 0; i < parsedJson['use_after'].length; i++) {
        NamedAPIResource item =
            NamedAPIResource.fromJson(parsedJson['use_after'][i]);
        useAfter.add(item);
      }
    } else {
      useAfter = null;
    }

    _useBefore = useBefore;
    _useAfter = useAfter;
  }

  List<NamedAPIResource>? get useBefore => _useBefore;
  List<NamedAPIResource>? get useAfter => _useAfter;
}

@JsonSerializable()
class ContestComboSets {
  late ContestComboDetail _normal;
  late ContestComboDetail _super;

  ContestComboSets(this._normal, this._super);

  ContestComboSets.fromJson(Map<String, dynamic> parsedJson) {
    _normal = ContestComboDetail.fromJson(parsedJson['normal']);
    _super = ContestComboDetail.fromJson(parsedJson['super']);
  }

  ContestComboDetail get normalCombo => _normal;
  ContestComboDetail get superCombo =>
      _super; // have to rename because we can't use super as variable name
}

@JsonSerializable()
class AbilityEffectChange {
  late List<Effect> _effectEntries;
  late NamedAPIResource _versionGroup;

  AbilityEffectChange(this._effectEntries, this._versionGroup);

  AbilityEffectChange.fromJson(Map<String, dynamic> parsedJson) {
    List<Effect> effectEntries = [];

    for (int i = 0; i < parsedJson['effect_entries'].length; i++) {
      Effect item = Effect.fromJson(parsedJson[i]['effect_entries']);
      effectEntries.add(item);
    }

    _effectEntries = effectEntries;
    _versionGroup = NamedAPIResource.fromJson(parsedJson['version_group']);
  }

  List<Effect> get effectEntries => _effectEntries;
  NamedAPIResource get versionGroup => _versionGroup;
}

@JsonSerializable()
class MoveFlavorText {
  late String _flavorText;
  late NamedAPIResource _language;
  late NamedAPIResource _versionGroup;

  MoveFlavorText(this._flavorText, this._language, this._versionGroup);

  MoveFlavorText.fromJson(Map<String, dynamic> parsedJson) {
    _flavorText = parsedJson['flavor_text'];
    _language = NamedAPIResource.fromJson(parsedJson['language']);
    _versionGroup = NamedAPIResource.fromJson(parsedJson['version_group']);
  }

  String get flavorText => _flavorText;
  NamedAPIResource get language => _language;
  NamedAPIResource get versionGroup => _versionGroup;
}

@JsonSerializable()
class MoveMetaData {
  late NamedAPIResource _ailment;
  late NamedAPIResource _category;
  late int? _minHits;
  late int? _maxHits;
  late int? _minTurns;
  late int? _maxTurns;
  late int _drain;
  late int _healing;
  late int _critRate;
  late int _ailmentChance;
  late int _flinchChance;
  late int _statChance;

  MoveMetaData(
    this._ailment,
    this._category,
    this._minHits,
    this._maxHits,
    this._minTurns,
    this._maxTurns,
    this._drain,
    this._healing,
    this._critRate,
    this._ailmentChance,
    this._flinchChance,
    this._statChance,
  );

  MoveMetaData.fromJson(Map<String, dynamic> parsedJson) {
    _ailment = NamedAPIResource.fromJson(parsedJson['ailment']);
    _category = NamedAPIResource.fromJson(parsedJson['category']);
    _minHits = parsedJson['min_hits'];
    _maxHits = parsedJson['max_hits'];
    _minTurns = parsedJson['min_turns'];
    _maxTurns = parsedJson['max_turns'];
    _drain = parsedJson['drain'];
    _healing = parsedJson['healing'];
    _critRate = parsedJson['crit_rate'];
    _ailmentChance = parsedJson['ailment_chance'];
    _flinchChance = parsedJson['flinch_chance'];
    _statChance = parsedJson['stat_chance'];
  }

  NamedAPIResource get ailment => _ailment;
  NamedAPIResource get category => _category;
  int? get minHits => _minHits;
  int? get maxHits => _maxHits;
  int? get minTurns => _minTurns;
  int? get maxTurns => _maxTurns;
  int get drain => _drain;
  int get healing => _healing;
  int get critRate => _critRate;
  int get ailmentChance => _ailmentChance;
  int get flinchChance => _flinchChance;
  int get statChance => _statChance;
}

@JsonSerializable()
class PastMoveStatValues {
  late int? _accuracy;
  late int? _effectChance;
  late int? _power;
  late int? _pp;
  late List<VerboseEffect> _effectEntries;
  late NamedAPIResource? _type;
  late NamedAPIResource _versionGroup;

  PastMoveStatValues(
    this._accuracy,
    this._effectChance,
    this._power,
    this._pp,
    this._effectEntries,
    this._type,
    this._versionGroup,
  );

  PastMoveStatValues.fromJson(Map<String, dynamic> parsedJson) {
    _accuracy = parsedJson['accuracy'];
    _effectChance = parsedJson['effect_chance'];
    _power = parsedJson['power'];
    _pp = parsedJson['pp'];
    _type = parsedJson['type'] != null
        ? NamedAPIResource.fromJson(parsedJson['type'])
        : null;
    _versionGroup = NamedAPIResource.fromJson(parsedJson['version_group']);

    List<VerboseEffect> effectEntries = [];

    for (int i = 0; i < parsedJson['effect_entries'].length; i++) {
      VerboseEffect item =
          VerboseEffect.fromJson(parsedJson[i]['effect_entries']);
      effectEntries.add(item);
    }

    _effectEntries = effectEntries;
  }

  int? get accuracy => _accuracy;
  int? get effectChance => _effectChance;
  int? get power => _power;
  int? get pp => _pp;
  List<VerboseEffect> get effectEntries => _effectEntries;
  NamedAPIResource? get type => _type;
  NamedAPIResource get versionGroup => _versionGroup;
}

@JsonSerializable()
class MoveStatChange {
  late int _change;
  late NamedAPIResource _stat;

  MoveStatChange(this._change, this._stat);

  MoveStatChange.fromJson(Map<String, dynamic> parsedJson) {
    _change = parsedJson['change'];
    _stat = NamedAPIResource.fromJson(parsedJson['stat']);
  }

  int get change => _change;
  NamedAPIResource get stat => _stat;
}

@JsonSerializable()
class GetPokemonMoveModel {
  late int _id;
  late String _name;
  late int? _accuracy;
  late int? _effectChance;
  late int _pp;
  late int _priority;
  late int? _power;
  late ContestComboSets? _contestCombos;
  late NamedAPIResource _contestType;
  late APIResource _contestEffect;
  late NamedAPIResource _damageClass;
  late List<VerboseEffect> _effectEntries;
  late List<AbilityEffectChange> _effectChanges;
  late List<MoveFlavorText> _flavorTextEntries;
  late NamedAPIResource _generation;
  late List<MachineVersionDetail> _machines;
  late MoveMetaData _meta;
  late List<Name> _names;
  late List<PastMoveStatValues> _pastValues;
  late List<MoveStatChange> _statChanges;
  late APIResource _superContestEffect;
  late NamedAPIResource _target;
  late NamedAPIResource _type;

  GetPokemonMoveModel(
    this._id,
    this._name,
    this._accuracy,
    this._effectChance,
    this._pp,
    this._priority,
    this._power,
    this._contestCombos,
    this._contestType,
    this._contestEffect,
    this._damageClass,
    this._effectEntries,
    this._effectChanges,
    this._flavorTextEntries,
    this._generation,
    this._machines,
    this._meta,
    this._names,
    this._pastValues,
    this._statChanges,
    this._superContestEffect,
    this._target,
    this._type,
  );

  GetPokemonMoveModel.fromJson(Map<String, dynamic> parsedJson) {
    _id = parsedJson['id'];
    _name = parsedJson['name'];
    _accuracy = parsedJson['accuracy'];
    _effectChance = parsedJson['effect_chance'];
    _pp = parsedJson['pp'];
    _priority = parsedJson['priority'];
    _power = parsedJson['power'];

    _contestCombos = parsedJson['contest_combos'] != null
        ? ContestComboSets.fromJson(parsedJson['contest_combos'])
        : null;

    _contestType = NamedAPIResource.fromJson(parsedJson['contest_type']);
    _contestEffect = APIResource.fromJson(parsedJson['contest_effect']);
    _damageClass = NamedAPIResource.fromJson(parsedJson['damage_class']);
    _generation = NamedAPIResource.fromJson(parsedJson['generation']);
    _meta = MoveMetaData.fromJson(parsedJson['meta']);
    _superContestEffect =
        APIResource.fromJson(parsedJson['super_contest_effect']);
    _target = NamedAPIResource.fromJson(parsedJson['target']);
    _type = NamedAPIResource.fromJson(parsedJson['type']);

    List<VerboseEffect> effectEntries = [];
    List<AbilityEffectChange> effectChanges = [];
    List<MoveFlavorText> flavorTextEntries = [];
    List<MachineVersionDetail> machines = [];
    List<Name> names = [];
    List<PastMoveStatValues> pastValues = [];
    List<MoveStatChange> statChanges = [];

    for (int i = 0; i < parsedJson['effect_entries'].length; i++) {
      VerboseEffect item =
          VerboseEffect.fromJson(parsedJson['effect_entries'][i]);
      effectEntries.add(item);
    }

    for (int i = 0; i < parsedJson['effect_changes'].length; i++) {
      AbilityEffectChange item =
          AbilityEffectChange.fromJson(parsedJson['effect_changes'][i]);
      effectChanges.add(item);
    }

    for (int i = 0; i < parsedJson['flavor_text_entries'].length; i++) {
      MoveFlavorText item =
          MoveFlavorText.fromJson(parsedJson['flavor_text_entries'][i]);
      flavorTextEntries.add(item);
    }

    for (int i = 0; i < parsedJson['machines'].length; i++) {
      MachineVersionDetail item =
          MachineVersionDetail.fromJson(parsedJson['machines'][i]);
      machines.add(item);
    }

    for (int i = 0; i < parsedJson['names'].length; i++) {
      Name item = Name.fromJson(parsedJson['names'][i]);
      names.add(item);
    }

    for (int i = 0; i < parsedJson['past_values'].length; i++) {
      PastMoveStatValues item =
          PastMoveStatValues.fromJson(parsedJson['past_values'][i]);
      pastValues.add(item);
    }

    for (int i = 0; i < parsedJson['stat_changes'].length; i++) {
      MoveStatChange item =
          MoveStatChange.fromJson(parsedJson['stat_changes'][i]);
      statChanges.add(item);
    }

    _effectEntries = effectEntries;
    _effectChanges = effectChanges;
    _flavorTextEntries = flavorTextEntries;
    _machines = machines;
    _names = names;
    _pastValues = pastValues;
    _statChanges = statChanges;
  }

  int get id => _id;
  String get name => _name;
  int? get accuracy => _accuracy;
  int? get effectChance => _effectChance;
  int get pp => _pp;
  int get priority => _priority;
  int? get power => _power;
  ContestComboSets? get contestCombos => _contestCombos;
  NamedAPIResource get contestType => _contestType;
  APIResource get contestEffect => _contestEffect;
  NamedAPIResource get damageClass => _damageClass;
  List<VerboseEffect> get effectEntries => _effectEntries;
  List<AbilityEffectChange> get effectChanges => _effectChanges;
  List<MoveFlavorText> get flavorTextEntries => _flavorTextEntries;
  NamedAPIResource get generation => _generation;
  List<MachineVersionDetail> get machines => _machines;
  MoveMetaData get meta => _meta;
  List<Name> get names => _names;
  List<PastMoveStatValues> get pastValues => _pastValues;
  List<MoveStatChange> get statChanges => _statChanges;
  APIResource get superContestEffect => _superContestEffect;
  NamedAPIResource get target => _target;
  NamedAPIResource get type => _type;
}
