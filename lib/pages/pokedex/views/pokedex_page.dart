import 'package:flutter/material.dart';
import 'package:pokedex_flutter/api/apiConfig.dart';
import 'package:pokedex_flutter/api/api_provider.dart';
import 'package:pokedex_flutter/constants/defaultValue.dart';
import 'package:pokedex_flutter/models/commonModel.dart';
import 'package:pokedex_flutter/models/getPokedexModel.dart';
import 'package:pokedex_flutter/pages/pokedex/views/pokemon_page.dart';
import 'package:pokedex_flutter/pages/pokedex/widgets/customPopupMenu.dart';
import 'package:pokedex_flutter/pages/pokedex/widgets/list_item.dart';

class PokedexPage extends StatefulWidget {
  @override
  _PokedexPageState createState() => _PokedexPageState();
}

class _PokedexPageState extends State<PokedexPage> {
  final _apiProvider = ApiProvider();

  final List<String> optionTitle = const ['Pokemon', 'Item', 'Location'];

  late String _queryType;
  late String _nextPage;
  late List<NamedAPIResource> _data;

  ScrollController scrollController = ScrollController();

  void _onPopupPressed(int index) {
    setState(() {
      _queryType = optionTitle[index];
      _nextPage = '';
      _data = [];
    });
  }

  void _onTitlePressed() {
    scrollController.animateTo(
      0.0,
      curve: Curves.easeOut,
      duration: const Duration(milliseconds: 1000),
    );
  }

  void _navigationHandler(String title, String url) {
    Navigator.pushNamed(
      context,
      'pokemon-page',
      arguments: PokemonPageParams(title, url),
    );
  }

  Future<GetPokedexModel> _getApiData(String type, String url) async {
    GetPokedexModel data;

    if (type == 'Pokemon') {
      data = GetPokedexModel.fromJson(
        await _apiProvider.getData(POKEMON_ENDPOINT),
      );
    } else if (type == 'Item') {
      data = GetPokedexModel.fromJson(
        await _apiProvider.getData(ITEM_ENDPOINT),
      );
    } else if (type == 'Location') {
      data = GetPokedexModel.fromJson(
        await _apiProvider.getData(LOCATION_ENDPOINT),
      );
    } else if (type == 'Url') {
      data = GetPokedexModel.fromJson(
        await _apiProvider.getDataByUrl(url),
      );
    } else {
      data = GetPokedexModel.fromJson(
        await _apiProvider.getData(POKEMON_ENDPOINT),
      );
    }

    _data = [..._data, ...data.results];
    _nextPage = data.next;

    return data;
  }

  Widget _renderPage() {
    return FutureBuilder<GetPokedexModel>(
      future: _getApiData(_queryType, _nextPage),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return ListItem(
            _data,
            _navigationHandler,
            scrollController,
          );
        } else if (snapshot.hasError) {
          return Center(
            child: Text('Data not found'),
          );
        } else
          return Center(
            child: CircularProgressIndicator(),
          );
      },
    );
  }

  @override
  void initState() {
    super.initState();

    setState(() {
      _queryType = 'Pokemon';
      _nextPage = '';
      _data = [];
    });

    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        setState(() {
          _queryType = 'Url';
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: CustomPopupMenu(
          icon: Icons.more_vert,
          title: optionTitle,
          length: optionTitle.length,
          onPressed: _onPopupPressed,
        ),
        title: TextButton(
          onPressed: _onTitlePressed,
          child: Text(
            'Pokédex',
            style: TextStyle(fontSize: 20),
          ),
          style: TextButton.styleFrom(
            primary: Colors.white,
          ),
        ),
        backgroundColor: PRIMARY_COLOUR,
      ),
      body: SafeArea(
        child: _renderPage(),
      ),
    );
  }
}
