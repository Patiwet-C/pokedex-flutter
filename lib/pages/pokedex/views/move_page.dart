import 'package:flutter/material.dart';
import 'package:pokedex_flutter/api/api_provider.dart';
import 'package:pokedex_flutter/constants/defaultValue.dart';
import 'package:pokedex_flutter/models/getPokemonMoveModel.dart';

class MovePageParams {
  final String title;
  final String url;

  MovePageParams(this.title, this.url);
}

class MovePage extends StatefulWidget {
  @override
  _MovePageState createState() => _MovePageState();
}

class _MovePageState extends State<MovePage> {
  @override
  Widget build(BuildContext context) {
    final route = ModalRoute.of(context)!.settings.arguments as MovePageParams;
    final ApiProvider _apiProvider = ApiProvider();

    Future<GetPokemonMoveModel> _getApiData() async {
      Map<String, dynamic> item = await _apiProvider.getDataByUrl(route.url);
      return GetPokemonMoveModel.fromJson(item);
    }

    Widget _renderTextRow(String title, String value) {
      return Row(
        children: <Widget>[
          Text(title),
          Text(value),
        ],
      );
    }

    Widget _renderList(GetPokemonMoveModel data) {
      print(data);
      return Column(
        children: <Widget>[
          Center(
            child: Text(
              route.title,
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          // Accuracy
          _renderTextRow('Accuracy:', data.accuracy.toString()),
          // Crit Rate
          _renderTextRow('Crit Rate:', data.meta.critRate.toString()),
          // Ailment
          _renderTextRow('Ailment:', data.meta.ailment.name.toString()),
          // Drain
          _renderTextRow('Drain:', data.meta.drain.toString()),
          // Healing
          _renderTextRow('Healing:', data.meta.healing.toString()),
          // Min Hits
          _renderTextRow('Min Hits:', data.meta.minHits.toString()),
          // Max Hits
          _renderTextRow('Max Hits:', data.meta.maxHits.toString()),
          // Min Turns
          _renderTextRow('Min Turns:', data.meta.minTurns.toString()),
          // Max Turns
          _renderTextRow('Max Turns:', data.meta.maxTurns.toString()),
        ],
      );
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: PRIMARY_COLOUR,
        title: Container(
          child: Text('${route.title} Detail'),
        ),
      ),
      body: SafeArea(
        child: FutureBuilder<GetPokemonMoveModel>(
          future: _getApiData(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Container(
                margin: EdgeInsets.all(16),
                child: _renderList(snapshot.data!),
              );
            } else if (snapshot.hasError) {
              return Center(
                child: Text('Data not found'),
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
      ),
    );
  }
}
