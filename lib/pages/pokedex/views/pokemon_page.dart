import 'package:flutter/material.dart';
import 'package:pokedex_flutter/api/api_provider.dart';
import 'package:pokedex_flutter/constants/defaultValue.dart';
import 'package:pokedex_flutter/models/getPokemonModel.dart';
import 'package:pokedex_flutter/pages/pokedex/views/move_page.dart';
import 'package:pokedex_flutter/pages/pokedex/widgets/pokemon_detail.dart';

class PokemonPageParams {
  final String title;
  final String url;

  PokemonPageParams(this.title, this.url);
}

class PokemonPage extends StatefulWidget {
  @override
  _PokemonPageState createState() => _PokemonPageState();
}

class _PokemonPageState extends State<PokemonPage> {
  @override
  Widget build(BuildContext context) {
    final ApiProvider _apiProvider = ApiProvider();
    final route =
        ModalRoute.of(context)!.settings.arguments as PokemonPageParams;

    Future<GetPokemonModel> _getApiData() async {
      Map<String, dynamic> item = await _apiProvider.getDataByUrl(route.url);

      return GetPokemonModel.fromJson(item);
    }

    void _navigationHandler(String title, String url) {
      Navigator.pushNamed(
        context,
        'move-page',
        arguments: MovePageParams(title, url),
      );
    }

    Widget _renderPage() {
      return FutureBuilder<GetPokemonModel>(
        future: _getApiData(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Container(
              padding: EdgeInsets.all(16),
              child: PokemonDetail(
                snapshot.data!,
                route.title,
                _navigationHandler,
              ),
            );
          } else if (snapshot.hasError) {
            return Center(
              child: Text('Data not found'),
            );
          } else
            return Center(
              child: CircularProgressIndicator(),
            );
        },
      );
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: PRIMARY_COLOUR,
        title: Text('${route.title} Detail'),
      ),
      body: SafeArea(
        child: _renderPage(),
      ),
    );
  }
}
