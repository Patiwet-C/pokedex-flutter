import 'package:flutter/material.dart';
import 'package:pokedex_flutter/models/commonModel.dart';
import 'package:pokedex_flutter/utils/formatter.dart';

class ListItem extends StatelessWidget {
  final List<NamedAPIResource> pokedex_flutterList;
  final Function(String title, String url) navigator;
  final ScrollController scrollController;

  ListItem(this.pokedex_flutterList, this.navigator, this.scrollController);

  Widget _renderItem(NamedAPIResource item) {
    String title = capitalFirstLetter(item.name);

    return ElevatedButton(
      style: ElevatedButton.styleFrom(
          onPrimary: Colors.black87,
          primary: Colors.white,
          padding: EdgeInsets.all(16)),
      onPressed: () {
        navigator(title, item.url);
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontSize: 16),
          ),
          Icon(
            Icons.arrow_forward_ios,
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      controller: scrollController,
      itemCount: pokedex_flutterList.length,
      itemBuilder: (context, index) {
        return _renderItem(pokedex_flutterList[index]);
      },
    );
  }
}
