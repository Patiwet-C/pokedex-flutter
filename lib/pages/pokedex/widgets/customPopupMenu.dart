import 'package:flutter/material.dart';

class CustomPopupMenu extends StatelessWidget {
  final List<String> title;
  final IconData icon;
  final int length;
  final Function(int index) onPressed;

  CustomPopupMenu({
    required this.title,
    required this.icon,
    required this.length,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<int>(
      onSelected: onPressed,
      child: Center(child: Icon(icon)),
      itemBuilder: (context) {
        return List.generate(length, (index) {
          return PopupMenuItem(
            value: index,
            child: Text(
              title[index],
              style: TextStyle(color: Colors.black87, fontSize: 14),
            ),
          );
        });
      },
    );
  }
}
