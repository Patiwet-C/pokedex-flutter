import 'package:flutter/material.dart';
import 'package:pokedex_flutter/models/getPokemonModel.dart';
import 'package:pokedex_flutter/utils/formatter.dart';

class PokemonDetail extends StatelessWidget {
  final GetPokemonModel data;
  final String title;
  final Function(String title, String url) onPressed;

  PokemonDetail(this.data, this.title, this.onPressed);

  final Map<String, String> _rowTitle = {
    'DETAIL': 'Details',
    'MOVE': 'Move List',
    'ABILITY': 'Ability List'
  };

  final Map<String, String> _detailTitle = {
    'EXP': 'base Experience:',
    'WEIGHT': 'weight:',
    'HEIGHT': 'height:',
  };

  final Map<String, String> _detailSubTitle = {
    'DECIMETRE': 'dm.',
    'HECTOGRAM': 'hg',
    'POINT': 'point.',
  };

  Widget _renderRowItem(String title, String value, String subTitle) {
    return Container(
      margin: EdgeInsets.only(bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              title,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
            ),
          ),
          Expanded(
            flex: 1,
            child: Text(
              value,
              style: TextStyle(fontSize: 16),
            ),
          ),
          Expanded(
            flex: 1,
            child: Text(
              subTitle,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              textAlign: TextAlign.right,
            ),
          )
        ],
      ),
    );
  }

  List<Widget> _renderMoveListItem() {
    List<Widget> items = [];

    for (PokemonMove move in data.moves) {
      Widget item = TextButton(
        style: TextButton.styleFrom(
          primary: Colors.black87,
        ),
        onPressed: () {
          onPressed(capitalFirstLetter(move.move.name), move.move.url);
        },
        child: Text(
          move.move.name,
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
        ),
      );

      items.add(item);
    }

    return items;
  }

  List<Widget> _renderAbilityListItem() {
    List<Widget> items = [];

    for (PokemonAbility ability in data.abilities) {
      Widget item = TextButton(
        style: TextButton.styleFrom(
          primary: Colors.black87,
        ),
        onPressed: () {
          print(ability.ability.url);
        },
        child: Text(
          ability.ability.name,
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
        ),
      );

      items.add(item);
    }

    return items;
  }

  List<Widget> _renderStatList() {
    List<Widget> items = [];

    for (PokemonStat stat in data.stats) {
      Widget item = _renderRowItem(
        stat.stat.name,
        stat.baseStat.toString(),
        _detailSubTitle['POINT']!,
      );

      items.add(item);
    }

    return items;
  }

  Widget _renderExpansionDetail(String title, List<Widget> children) {
    return ExpansionTile(
      title: Text(
        title,
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.bold,
        ),
      ),
      children: children,
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Text(
            title,
            style: TextStyle(
              fontSize: 32,
              fontWeight: FontWeight.bold,
            ),
          ),
          Container(
            alignment: Alignment.center,
            child: Image.network(
              data.sprites.frontDefault!,
              scale: 0.3,
            ),
          ),
          // details list
          _renderExpansionDetail(
            _rowTitle['DETAIL']!,
            [
              _renderRowItem(
                _detailTitle['EXP']!,
                data.baseExperience.toString(),
                _detailSubTitle['POINT']!,
              ),
              _renderRowItem(
                _detailTitle['WEIGHT']!,
                data.weight.toString(),
                _detailSubTitle['HECTOGRAM']!,
              ),
              _renderRowItem(
                _detailTitle['HEIGHT']!,
                data.height.toString(),
                _detailSubTitle['DECIMETRE']!,
              ),
              ..._renderStatList(),
            ],
          ),
          //  move list
          _renderExpansionDetail(
            _rowTitle['MOVE']!,
            _renderMoveListItem(),
          ),
          // ability list
          _renderExpansionDetail(
            _rowTitle['ABILITY']!,
            _renderAbilityListItem(),
          ),
        ],
      ),
    );
  }
}
