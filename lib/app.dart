import 'package:flutter/material.dart';
import 'package:pokedex_flutter/routes/routes.dart';

class App extends MaterialApp {
  App() : super(home: Routes());
}
